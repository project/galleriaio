/**
 * @file
 * Galleria behavior.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Provide galleria.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the galleria.
   */
  Drupal.behaviors.galleria = {
    attach: function (context, settings) {

      // if (!Galleria) {
      //   throw new Error('Galleria object not found');
      // }

      var settings = settings.galleria;
      Galleria.loadTheme(settings.theme);
      settings.options['height'] = 500;
      Galleria.configure(settings.options);

      console.log(settings.options);

      Galleria.run('.galleria');
    }
  };

})(jQuery, Drupal, drupalSettings);
