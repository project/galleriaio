<?php

namespace Drupal\galleriaio;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Provides a list controller for content_entity_example_contact entity.
 *
 * @ingroup galleriaio
 */
class GalleriaListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = $this->t('Title');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['title'] = $entity->label();;
    return $row + parent::buildRow($entity);
  }

}