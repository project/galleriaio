<?php

namespace Drupal\galleriaio\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Galleria item annotation object.
 *
 * @see \Drupal\galleriaio\Plugin\GalleriaManager
 * @see plugin_api
 *
 * @Annotation
 */
class Galleria extends Plugin {

  /**
   * The plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
