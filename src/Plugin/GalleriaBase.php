<?php

namespace Drupal\galleriaio\Plugin;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Utility\Unicode;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides a base class for GalleriaBase plugins.
 *
 * @see \Drupal\galleriaio\Annotation\Galleria
 * @see \Drupal\galleriaio\Plugin\GalleriaManager
 * @see \Drupal\galleriaio\Plugin\GalleriaInterface
 * @see plugin_api
 */
abstract class GalleriaBase extends PluginBase implements GalleriaInterface {

  public function render($options) {
    return [
      '#markup' => '<div class="galleria"></div>',
      '#attached' => [
        'library' => 'galleriaio/main',
        'drupalSettings' => [
          'galleria' => [
            'theme' => $this->getThemePath(),
            'options' => $options,
          ],
        ],
      ],
    ];
  }

}
