<?php

namespace Drupal\galleriaio\Plugin\Field\FieldFormatter;

use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\galleriaio\Plugin\GalleriaManager;
use Drupal\Core\Field\FormatterBase;
use Drupal\galleriaio\Form\GalleriaSettingsFormTrait;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;

/**
 * Plugin implementation of the 'galleria' formatter.
 *
 * @FieldFormatter(
 *   id = "galleria_image",
 *   label = @Translation("Galleria"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class GalleriaDefaultFormatter extends FileFormatterBase implements ContainerFactoryPluginInterface {

  use GalleriaSettingsFormTrait;

  const EXTENSIONS = [
    'png',
    'gif',
    'jpg',
    'jpeg',
  ];

  /**
   * The plugin manager.
   *
   * @var \Drupal\galleriaio\Plugin\GalleriaManager
   */
  protected $pluginManager;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\galleriaio\Plugin\GalleriaManager $plugin_manager
   *   The galleria manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, GalleriaManager $plugin_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.galleria')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'theme' => 'classic',
      'height' => 500,
      'options' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $this->getSettingsForm($element);

    $element['theme']['#default_value'] = $this->getSetting('theme');
    $element['height']['#default_value'] = $this->getSetting('height');
    $element['options']['#default_value'] = $this->getSetting('options');

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $entities = $this->getEntitiesToView($items, $langcode);
    $options = [];

    foreach ($entities as $entity) {
      $options['dataSource'][] = [ 
        'image' => $entity->url(),
      ];
    }

    $options['height'] = (int) $this->getSetting('height');

    return $this->pluginManager->createInstance($this->getSetting('theme'))->render($options);
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {    
    $extensions = explode(' ', $field_definition->getSetting('file_extensions'));
    $access = FALSE;

    foreach (static::EXTENSIONS as $extension) {
      if (in_array($extension, $extensions)) {
        $access = TRUE;
      }
    }

    return $access;
  }

}
