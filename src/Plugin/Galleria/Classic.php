<?php

namespace Drupal\galleriaio\Plugin\Galleria;

use Drupal\galleriaio\Plugin\GalleriaBase;

/**
 * Class Classic.
 *
 * @Galleria(
 *   id = "classic",
 *   label = @Translation("Classic")
 * )
 */
class Classic extends GalleriaBase {
   
  public function getThemePath() {
    return '/modules/contrib/galleriaio/themes/classic/galleria.classic.min.js';
  }

}
