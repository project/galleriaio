<?php

namespace Drupal\galleriaio\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Galleria manager interface.
 */
interface GalleriaInterface {

  public function getThemePath();

}
