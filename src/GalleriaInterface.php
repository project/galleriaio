<?php

namespace Drupal\galleriaio;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Contact entity.
 *
 * @ingroup galleriaio
 */
interface GalleriaInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {
}