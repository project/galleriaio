<?php

namespace Drupal\galleriaio\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\galleriaio\GalleriaInterface;
use Drupal\user\UserInterface;
use Drupal\file\Entity\File;

/**
 * @ContentEntityType(
 *   id = "galleria",
 *   label = @Translation("Galleria entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\galleriaio\GalleriaViewBuilder",
 *     "list_builder" = "Drupal\galleriaio\GalleriaListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\galleriaio\Entity\Form\GalleriaForm",
 *       "edit" = "Drupal\galleriaio\Entity\Form\GalleriaForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "access" = "Drupal\galleriaio\Entity\Access\GalleriaAccessControlHandler",
 *   },
 *   base_table = "galleria",
 *   admin_permission = "administer galleria entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "map" = "data"
 *   },
 *   links = {
 *     "canonical" = "/galleria/{galleria}",
 *     "edit-form" = "/galleria/{galleria}/edit",
 *     "delete-form" = "/galleria/{galleria}/delete",
 *     "collection" = "/galleria/list"
 *   }
 * )
 */
class Galleria extends ContentEntityBase implements GalleriaInterface {

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setChangedTime($timestamp) {
    $this->set('changed', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTimeAcrossTranslations()  {
    $changed = $this->getUntranslated()->getChangedTime();
    foreach ($this->getTranslationLanguages(FALSE) as $language)    {
      $translation_changed = $this->getTranslation($language->getId())->getChangedTime();
      $changed = max($translation_changed, $changed);
    }
    return $changed;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  public function getContent() {
    $content = current($this->get('data')->getValue());
    if ($content && is_array($content)) {
      return $content;
    }

    return NULL;
  }

  public function getTheme() {
    return 'classic';
  }

  public function getOptions() {
    $options = [];
    $map = [
      'image_uri' => 'image',
      'video' => 'video',
      'image' => 'image',
    ];

    foreach ($this->getContent() as $content) {
      $options['dataSource'][] = [
        $map[$content['type']] => $content['content'], 
      ];
    }

    array_walk($options['dataSource'], function (&$option) {
      if (isset($option['image']) && is_array($option['image'])) {
        $entity = File::load(current($option['image']));
        $option['image'] = $entity->url();
      }
    });

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Galleria entity.'))
      ->setReadOnly(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -6,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -6,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Map'));

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code of Contact entity.'));
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }
}
