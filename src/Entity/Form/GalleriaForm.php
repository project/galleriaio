<?php

namespace Drupal\galleriaio\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Language\Language;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\Component\Utility\NestedArray;

/**
 * Form controller for the galleria entity edit forms.
 *
 * @ingroup galleriaio
 */
class GalleriaForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    /* @var $entity \Drupal\galleriaio\Entity\Galleria */
    $entity = $this->entity;

    $form['#tree'] = TRUE;

    $form['content'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'content-container',
      ],
    ];

    if (!$form_state->get('content')) {
      $form_state->set('content', $entity->getContent() ?? []);
    }

    $content = $form_state->get('content');

    foreach ($content as $id => $data) {
      switch ($data['type']) {
        case 'image':
          $form['content'][$id]['image'] = [
            '#type' => 'managed_file',
            '#title' => $this->t('Image'),
            '#weight' => $id,
            '#default_value' => $data['content'],
          ];
          break;
        case 'image_uri':
          $form['content'][$id]['image_uri'] = [
            '#type' => 'url',
            '#title' => $this->t('Image URL'),
            '#weight' => $id,
            '#default_value' => $data['content'],
          ];
          break;
        case 'video':
          $form['content'][$id]['video'] = [
            '#type' => 'url',
            '#title' => $this->t('Video URL'),
            '#weight' => $id,
            '#default_value' => $data['content'],
          ];
          break;
      }
    }

    $actions = [
      'image' => $this->t('Add image'),
      'image_uri' => $this->t('Add image from URL'),
      'video' => $this->t('Add video'),
    ];

    $form['container'] = [
      '#type' => 'container',
    ];

    foreach ($actions as $type => $action) {
      $form['container']['actions'][$type] = [
        '#type' => 'submit',
        '#value' => $action,
        '#submit' => ['::addSubmit'],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => '::addMoreAjaxForm',
          'wrapper' => 'content-container',
        ],
      ];
    }

    return $form;
  }

  public function addMoreAjaxForm(array &$form, FormStateInterface $form_state) : array {
    return $form['content'];
  }

  public function addSubmit(array &$form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    $content = $form_state->get('content');
    $content[count($content) + 1] = [
      'type' => end($button['#array_parents']),
      'content' => [],
    ];

    $form_state->set('content', $content);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    parent::copyFormValuesToEntity($entity, $form, $form_state);

    $content = $form_state->getValues()['content'];
    $data = [];
    foreach ($content as $id => $content) {
      $data[$id] = [
        'type' => key($content),
        'content' => current($content),
      ];
    }

    $entity->set('data', serialize($data));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    $entity = $this->entity;

    if ($status == SAVED_UPDATED) {
      drupal_set_message($this->t('The galleria %galleria has been updated.', ['%galleria' => $entity->toLink()->toString()]));
    } else {
      drupal_set_message($this->t('The galleria %galleria has been added.', ['%galleria' => $entity->toLink()->toString()]));
    }

    $form_state->setRedirectUrl($this->entity->toUrl('canonical'));

    return $status;
  }
}

?>