<?php

namespace Drupal\galleriaio\Form;

use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Implements an example form.
 */
trait GalleriaSettingsFormTrait {

  public function getSettingsForm(array &$form) {
    $form['theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose theme'),
      '#options' => $this->pluginManager->getDefinitionsMap(),
      '#required' => TRUE,
    ];

    $form['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#required' => TRUE,
      '#description' => $this->t('Set height for container.'),
      '#min' => 0,
    ];

    $form['options'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Options'),
      '#description' => $this->t('See list of available options <a href="@uri">here</a>.', [
        '@uri' => OPTION_LIST_URI, 
      ]),
    ];

    return $form;
  }

}
