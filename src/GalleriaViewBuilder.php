<?php

namespace Drupal\galleriaio;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Theme\Registry;

/**
 * Galleria entity view builder.
 *
 * @ingroup galleriaio
 */
class GalleriaViewBuilder extends EntityViewBuilder {

  /**
   * The plugin manager.
   *
   * @var \Drupal\galleriaio\Plugin\GalleriaManager
   */
  protected $plugin;

  /**
   * Constructs a new GalleriaViewBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Theme\Registry $theme_registry
   *   The theme registry.
   * @param \Drupal\galleriaio\Plugin\GalleriaManager $plugin
   *   The plugin manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityManagerInterface $entity_manager, LanguageManagerInterface $language_manager, Registry $theme_registry = NULL, $plugin) {
  	parent::__construct($entity_type, $entity_manager, $language_manager, $theme_registry);

  	$this->plugin = $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
    $entity_type, 
    $container->get('entity.manager'), 
    $container->get('language_manager'), 
    $container->get('theme.registry'),
    $container->get('plugin.manager.galleria')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    $build += $this->plugin->createInstance($entity->getTheme())->render($entity->getOptions());

    return $build;
  }
}
